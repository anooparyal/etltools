#!/usr/bin/env python

import csv, sys, re, os
import codecs

infile = sys.argv[1]
outfile = sys.argv[2]

def clean(c):
    c = c.strip()
    return c

with codecs.open(infile, 'r', 'utf-8', errors='ignore') as inf, codecs.open(outfile, 'w', 'utf-8') as outf:
    c = csv.reader(inf)
    cout = csv.writer(outf)
    for row in c:
        row = [clean(cell) for cell in row]
        cout.writerow(row)
        

