#!/usr/bin/env python

import csv, sys, re, os
import codecs
from operator import itemgetter
from pprint import pprint

tests = [
    ("integer", re.compile("^[0-9]+$")),
    ("numeric", re.compile("^[-+]?\d+\.\d+$")),
    ("timestamp with time zone", re.compile("^[0-9][0-9]/[0-9][0-9]/[0-9][0-9] [0-9][0-9]:[0-9][0-9]:[0-9][0-9]$")),
    ("varchar(255)", re.compile("^.{1,255}$")),
    ("varchar(1024)", re.compile("^.{1,1024}$")),
    ("text", re.compile("^.+$"))
]

type_priority = dict([(name, p) for p, name in list(enumerate([x for x,y in tests]))])

file = sys.argv[1]

def guess_type(x):
    for name, t in tests:
        if t.match(x):
            return name
    return None

def guess_types(vals):
    t = set([guess_type(x) for x in vals])
    if None in t:
        t.remove(None)
    return t

def getMostPermissive(types):
    p = [(type_priority.get(t),t) for t in types]
    p_by_priority = sorted(p, key=itemgetter(0), reverse=True)
    return next(iter(p_by_priority))[1]


fields = []
with codecs.open(file, 'r', 'utf-8', errors='ignore') as f:
    c = csv.DictReader(f)
    headers = c.fieldnames
    records = [row for row in c]

    
    for field in headers:
        vals = [r[field] for r in records]
        t = guess_types(vals)
        fields.append((field, t))

flds = []
for name,ftype in fields:
    ftype = list(ftype)
    if len(ftype) == 0:
        distinct_values = list(set([x[name].strip() for x in records]))
        if len(distinct_values) != 1:
            print("Non empty field '%s'. Can't guess type. Values: '%s'" % (name, "','".join(distinct_values)))
        ftype.append("varchar(255)")
    #flds.append("\t%s %s" % (name," ".join(ftype)))
    flds.append("\t%s %s" % (name,getMostPermissive(ftype)))
    
tableName = file.split(".")[0].split("/")[-1]
print("drop table if exists",tableName,";")
print("create table %s (" % tableName)
print("id serial not null primary key,")
print(",\n".join(flds))
print(");")

fieldNames = [fld.split(" ")[0].strip() for fld in flds]

fullFileName = os.path.abspath(file)
print("-- COPY %s (%s) FROM '%s' DELIMITER ',' CSV HEADER;" % (tableName, ",".join(fieldNames), fullFileName))
print("\copy %s (%s) FROM '%s' DELIMITER ',' CSV HEADER;" % (tableName, ",".join(fieldNames), fullFileName))

