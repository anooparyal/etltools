# mdb-tables will dump all the data/tables into csv files.
CSV_DIR=data/csv
CLEAN_CSV_DIR=data/clean_csv
SQL_DIR=data/sql
CSV_FILES=$(wildcard $(CSV_DIR)/*.csv)
CLEAN_CSV_FILES=$(addprefix $(CLEAN_CSV_DIR)/, $(notdir $(CSV_FILES:.csv=.sql)))
DDL_FILES=$(addprefix $(SQL_DIR)/, $(notdir $(CSV_FILES:.csv=.sql)))
LD_FILES=$(DDL_FILES:.sql=.ld)
PSQL_OPTS=-h tillable-db.cdfou00ji4ka.us-east-2.rds.amazonaws.com -U postgres farmdb

all: $(LD_FILES)

.SUFFIXES:

.SUFFIXES: .csv .sql

$(CLEAN_CSV_DIR)/%.csv: $(CSV_DIR)/%.csv
	./bin/clean_csv.py $< $@

$(SQL_DIR)/%.sql: $(CLEAN_CSV_DIR)/%.csv
	./bin/csv_to_psql.py $< > $@

%.ld: %.sql
	psql $(PSQL_OPTS) -v ON_ERROR_STOP=1 -f $< && touch $@

clean:
	rm $(DDL_FILES) $(LD_FILES)

.PHONY: all

.PRECIOUS: $(CLEAN_CSV_FILES) $(DDL_FILES)

